import { connect } from 'react-redux';

import AmendmentList from '../component/AmendmentList';

const mapStateToProps = state => {
  return {
    amendments: state.amendmentsByProject[state.selectedProject],
  }
};

export default connect(mapStateToProps)(AmendmentList);
