import { connect } from 'react-redux';

import ArticleList from '../component/ArticleList';

const mapStateToProps = state => {
  return {
    articles: state.articlesByProject[state.selectedProject],
    articleContentById: state.articleContentById,
  }
};

export default connect(mapStateToProps)(ArticleList);
