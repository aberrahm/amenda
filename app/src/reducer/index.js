import { combineReducers } from 'redux';
import selectedProject from './selectedProject';
import articlesByProject from './articlesByProject';
import articleContentById from './articleContentById';
import amendmentsByProject from './amendmentsByProject';
import user from './user';

export default combineReducers({
  selectedProject,
  articlesByProject,
  articleContentById,
  amendmentsByProject,
  user,
});
