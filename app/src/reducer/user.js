import {
  REQUEST_USER,
  RECEIVE_USER,
} from '../action/user';

export default function user(
  state = {
    isFetching: false,
    didInvalidate: false,
    data: null,
  },
  action
) {
  switch (action.type) {
    case REQUEST_USER:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_USER:
      return {
        ...state,
        ...action.user,
        isFetching: false,
      };
    default:
      return state;
  }
}
