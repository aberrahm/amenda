export const REQUEST_USER = 'REQUEST_USER';
export const RECEIVE_USER = 'RECEIVE_USER';

export function requestUser() {
  return {
    type: REQUEST_USER,
  }
}

export function receiveUser(user) {
  return {
    type: RECEIVE_USER,
    user,
  }
}

export function fetchUser() {
  return dispatch => {
    dispatch(requestUser());
    return fetch(`/api/v1/user`, {credentials: 'same-origin'})
      .then(response => response.json())
      .then(json => dispatch(receiveUser(json)));
  }
}

export function shouldFetchUser(state) {
  const user = state.user;

  if (!user || !user.data) {
    return true;
  } else if (user.isFetching) {
    return false;
  } else {
    return user.didInvalidate;
  }
}

export function fetchUserIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchUser(getState())) {
      return dispatch(fetchUser());
    }
  }
}
