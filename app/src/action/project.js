export const INVALIDATE_PROJECT = 'INVALIDATE_PROJECT';
export const SELECT_PROJECT = 'SELECT_PROJECT';

export function selectProject(project) {
  return {
    type: SELECT_PROJECT,
    project,
  }
}

export function invalidateProject(project) {
  return {
    type: INVALIDATE_PROJECT,
    project,
  }
}
