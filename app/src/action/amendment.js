export const REQUEST_AMENDMENTS = 'REQUEST_AMENDMENTS';
export const RECEIVE_AMENDMENTS = 'RECEIVE_AMENDMENTS';

function requestAmendments(project) {
  return {
    type: REQUEST_AMENDMENTS,
    project,
  };
}

function receiveAmendments(project, amendments) {
  return {
    type: RECEIVE_AMENDMENTS,
    project,
    amendments,
    receivedAt: Date.now()
  };
}

function fetchAmendments(project) {
  return dispatch => {
    dispatch(requestAmendments(project));
    return fetch(`/api/v1/project/${project}/amendment`, {credentials: 'same-origin'})
      .then(response => response.json())
      .then(json => dispatch(receiveAmendments(project, json)));
  }
}

function shouldFetchAmendments(state, projectId) {
  return !state.amendmentsByProject[projectId].items
    && !state.amendmentsByProject[projectId].isFetching;
}

export function fetchAmendmentsIfNeeded(project) {
  return (dispatch, getState) => {
    if (shouldFetchAmendments(getState(), project)) {
      return dispatch(fetchAmendments(project));
    }
  }
}
