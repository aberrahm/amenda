import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'react-toolbox/lib/list';

import Article from './Article';

import {
  fetchArticlesIfNeeded,
  fetchArticleContentIfNeeded,
} from '../action/article';

class ArticleList extends Component {
  componentDidUpdate() {
    const { dispatch, project } = this.props;

    if (!project) {
      return ;
    }

    // FIXME: hardcoded ref to "master"
    dispatch(fetchArticlesIfNeeded(project, 'master'));
  }

  handleArticleClick(article) {
    const { dispatch, project } = this.props;

    // FIXME: hardcoded ref to "master"
    dispatch(fetchArticleContentIfNeeded(project, 'master', article.id));
  }

  render() {
    // FIXME: hardcoded ref to "master"
    if (!this.props.project
        || !this.props.articles
        || !this.props.articles['master']
        || !Array.isArray(this.props.articles['master'].items)) {
      return null;
    }

    return (
      <List selectable ripple>
        {this.props.articles['master'].items.map((article) =>
          <Article
            key={article.id}
            name={article.name}
            content={this.props.articleContentById[article.id]}
            onClick={() => this.handleArticleClick(article)}
          />
        )}
      </List>
    );
  }
}

ArticleList.propTypes = {
  project: PropTypes.number.isRequired,
};

export default ArticleList;
