import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Tab from 'react-toolbox/lib/tabs/Tab';
import Tabs from 'react-toolbox/lib/tabs/Tabs';
import Layout from 'react-toolbox/lib/layout/Layout';
import Panel from 'react-toolbox/lib/layout/Panel';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';

import '../asset/react-toolbox/theme.css';
import '../asset/font/material-icons.css';
import '../asset/font/roboto.css';
import './App.css';

import theme from '../asset/react-toolbox/theme';
import ArticleListContainer from '../container/ArticleListContainer';
import HeaderContainer from '../container/HeaderContainer';
import AmendmentListContainer from '../container/AmendmentListContainer';

import {
  selectProject,
} from '../action/project';

class App extends Component {
  state = {
    index: 0,
    user: null,
  };

  componentWillMount() {
    const { dispatch } = this.props;
    // Single project app for now.
    // FIXME: should actually list the user's projects from the API and use the first one
    dispatch(selectProject(1));
  }

  handleTabChange = (index) => {
    this.setState({
      index,
    });
  };

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Layout>
          <Panel>
            <HeaderContainer/>
            <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
              <Tabs
                index={this.state.index}
                onChange={this.handleTabChange}>
                <Tab label='Projet de loi'>
                  <ArticleListContainer project={this.props.selectedProject}/>
                </Tab>
                <Tab label='Amendements' onActive={this.handleActive}>
                  <AmendmentListContainer project={this.props.selectedProject}/>
                </Tab>
              </Tabs>
            </div>
          </Panel>
        </Layout>
      </ThemeProvider>
    );
  }
}

App.propTypes = {
  selectedProject: PropTypes.number,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const { selectedProject } = state;

  return {
    selectedProject,
  };
}

export default connect(mapStateToProps)(App);
