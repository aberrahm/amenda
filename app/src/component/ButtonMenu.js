import React, { Component } from 'react';

import Menu from 'react-toolbox/lib/menu/Menu';
import Button from 'react-toolbox/lib/button/Button';

export default class ButtonMenu extends Component {
  state = { active: false };
  handleButtonClick = () => this.setState({ active: !this.state.active });
  handleMenuHide = () => this.setState({ active: false });
  render () {
    return (
      <div style={{ display: 'inline-block', position: 'relative' }}>
        <Button primary raised onClick={this.handleButtonClick} label={this.props.label} icon={this.props.icon} />
        <Menu position="topRight" active={this.state.active} onHide={this.handleMenuHide}>
          {this.props.children}
        </Menu>
      </div>
    );
  }
}
