import React, { Component } from 'react';
import { List, ListItem } from 'react-toolbox/lib/list';

import {
  fetchAmendmentsIfNeeded,
} from '../action/amendment';

export default class AmendmentList extends Component {
  componentDidMount() {
    const { dispatch, project } = this.props;
    
    if (!project) {
      return ;
    }
    
    dispatch(fetchAmendmentsIfNeeded(project));
  }

  componentDidUpdate() {
    const { dispatch, project } = this.props;
    
    if (!project) {
      return ;
    }
    
    dispatch(fetchAmendmentsIfNeeded(project));
  }
  
  render() {
    if (!this.props.amendments || !this.props.amendments.items) {
      return null;
    }

    return (
      <List selectable ripple>
        {
          this.props.amendments.items.map((amendement) =>
            <ListItem
              key={amendement.id}
              caption={amendement.title}
              legend={amendement.description}
              // leftIcon={amendement.status}
              onClick={() => this.props.onAmendmentClick(amendement.id)}
            />
          )
        }
      </List>
    );
  }
}
