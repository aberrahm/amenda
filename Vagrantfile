Vagrant.require_version ">= 2.0.0"

Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/xenial64"

  config.vm.provider :virtualbox do |vbox, override|
    vbox.customize ["modifyvm", :id, "--memory", ENV['VAGRANT_MEMORY'] || 4096]
    vbox.customize ["modifyvm", :id, "--cpus", ENV['VAGRANT_NUM_CORES'] || 2]

    # Allow symlinks in the /vagrant shared folder on Windows.
    # https://github.com/npm/npm/issues/7308#issuecomment-84214837
    vbox.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
  end

  config.vm.network :private_network, ip: "192.168.50.42"

  config.vm.provision "ansible_local" do |ansible|
    ansible.verbose = ENV['ANSIBLE_VERBOSE'] || false
    ansible.tags = ENV['ANSIBLE_TAGS'] || false
    ansible.skip_tags = ENV['ANSIBLE_SKIP_TAGS'] || false
    ansible.compatibility_mode = "2.0"
    ansible.version = "2.4.0.0"
    ansible.install_mode = "pip"
    ansible.playbook = "provisioning/playbook.yml"
    ansible.galaxy_role_file = "provisioning/requirements.yml"
    ansible.galaxy_roles_path = "/etc/ansible/roles"
    ansible.galaxy_command = "sudo ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path}"
    ansible.become = true
    ansible.become_user = "root"
  end
end
