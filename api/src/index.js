const app = require('express')();
const bodyParser = require('body-parser');
const proxy = require('express-http-proxy');
const querystring = require('querystring');
const cookieSession = require('cookie-session');
const nunjucks = require('nunjucks');
const request = require('superagent');
const async = require('async');

nunjucks.configure('./src', {
  autoescape: true,
  express: app,
  noCache: true,
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieSession({
  name: 'session',
  keys: [
    '42', // FIXME: generate actual keys (during provisioning?)
  ],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

app.use((req, res, next) => {
  res.apiResponse = (err, obj, template) => {
    if (!!err) {
      console.log(err);
      res.status(err.status).send(err);
    } else {
      res.send(JSON.parse(nunjucks.render(template, obj)));
    }
  }
  next();
});

app.use((req, res, next) => {
  // The REST API must be stateless. But when we call it from the Web client,
  // we want the session cookie to be enough to authenticate the user.
  // In this middleware, we emulate this by passing the access_token stored in
  // the session cookie as if it was set in the GET query parameters.
  if (!!req.session && !!req.session.access_token) {
    req.query.access_token = req.session.access_token;
  }

  next();
});

function proxyAuthorizationHeader(proxyReqOpts, srcReq) {
  proxyReqOpts.headers = {
    ...proxyReqOpts.headers,
    ...getRequestAuthorizationHeader(srcReq),
  };

  return proxyReqOpts;
}

function getRequestAuthorizationHeader(req) {
  // If there is an Authorization HTTP header, use it.
  if (!!req.headers['Authorization']) {
    return {'Authorization': req.headers['Authorization']};
  }
  // Else, if we have an access_token in the query, use it.
  // This method is simpler than adding the access_token GET parameter to all
  // queries.
  // https://docs.gitlab.com/ce/api/oauth2.html#access-gitlab-api-with-access-token
  else if (!!req.query.access_token) {
    return {'Authorization': 'Bearer ' + req.query.access_token};
  }
  // Else, if we have a private_token, use it.
  else if (!!req.query.private_token) {
    return {'Private-Token': req.query.private_token};
  }

  return {};
}

/**
 * @api {get} /api/v1/ping Ping
 * @apiVersion 1.0.0
 * @apiDescription Test if the API server is running/responding.
 * @apiName Ping
 * @apiGroup Utils
 *
 * @apiSuccess {String} ping pong
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {"ping":"pong"}
 */
app.use('/api/v1/ping', require('./ping'));

app.get(
  '/api/v1/project',
  (req, res) => request.get(
      'http://localhost:8000/admin/api/v4/'
      + '/projects/'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'project_array.njk'))
);

/**
 * @api {get} /api/v1/project/:project_id/:amendment/articles List articles
 * @apiVersion 1.0.0
 * @apiDescription List all articles for the specified project.
 * @apiName List articles
 * @apiGroup Article
 *
 * @apiSuccess {String} id ID of the file
 * @apiSuccess {String} name Name of the file
 * @apiSuccess {String} type Type of the file
 * @apiSuccess {String} path Path of the file
 * @apiSuccess {String} mode Mode of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": "fc798eff13af1df4a91dc14d26b22c95a90e717d",
 *         "name": "Article_1.md",
 *       }
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id/article',
  (req, res) => async.waterfall(
    [
       // fetch the amendment merge request
       (callback) => request.get(
            'http://localhost:8000/admin/api/v4/'
            + '/projects/' + req.params.project_id
            + '/merge_requests/' + req.params.amendment_id
          )
          .set(getRequestAuthorizationHeader(req))
          .end((err, res) => callback(err, res.body)),
      // fetch articles for the corresponding source branch
      (mergeRequest, callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/repository/tree?ref=' + mergeRequest.source_branch
        )
        .set(getRequestAuthorizationHeader(req))
        .end(callback),
    ],
    (err, gitlabRes) => {
      res.apiResponse(err, gitlabRes, 'tree.njk');
    }
  )
);

app.get(
  '/api/v1/project/:project_id/article',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/repository/tree?ref=master'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'tree.njk'))
);

/**
 * @api {get} /project/:project_id/:amendment/:file_path Get article content
 * @apiVersion 1.0.0
 * @apiDescription Get an article content
 * @apiName Get article content
 * @apiGroup Article
 *
 * @apiSuccess {String} content of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       Ceci est le premier article.
 *     ]
 */

app.get(
  '/api/v1/project/:project_id/article/:article_id',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/repository/files/Article_' + req.params.article_id + '.md'
      + '/raw?ref=master'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text))
);

app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id/article/:article_id',
  (req, res) => async.waterfall(
    [
      // fetch the merge request
      (callback) => request.get(
          'http://localhost:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/merge_requests/' + req.params.amendment_id
        )
        .set(getRequestAuthorizationHeader(req))
        .end((err, res) => callback(err, res.body)),
      // fetch the article file
      (mergeRequest, callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/repository/files/Article_' + req.params.article_id + '.md'
          + '/raw?ref=' + mergeRequest.source_branch
        )
      .set(getRequestAuthorizationHeader(req))
      .end(callback)
    ],
    (err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text)
  )
);

/**
 * @api {put} /project/:project_id/:amendment/:file_path Set an article content
 * @apiVersion 1.0.0
 * @apiDescription Set an article content
 * @apiName Set article article
 * @apiGroup Article
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (URL) {String} project_id the path of the update article
 * @apiParam (URL) {String} amendment the amendment name
 * @apiParam (URL) {String} article_path the article path
 * @apiParam (POST) {String} content the new content of the article
 * @apiParam (POST) {String} commit_message the commit message
 *
 * @apiSuccess {String} file_path path of the modified article
 * @apiSuccess {String} branch the amendment name
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "file_path": "Article_42.md",
 *       "branch": "master"
 *     }
 */
app.put('/api/v1/project/:project_id/:amendment_id/article/:article_id',
  (req, res) => async.waterfall(
    [
      // fetch the merge request
      (callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/merge_requests/' + req.params.amendment_id
        )
        .set(getRequestAuthorizationHeader(req))
        .end((err, res) => callback(err, res.body)),
      // FIXME: make sure the MR has the "draft" label
      // commit on the merge request's source branch
      (mergeRequest, callback) => request.put(
        'http://127.0.0.1:8000/admin/api/v4'
        + '/projects/' + req.params.project_id
        + '/repository/files/Article_' + req.params.article_id + '.md'
        + '?branch=' + mergeRequest.source_branch
      )
      .set(getRequestAuthorizationHeader(req))
      .end(callback),
    ],
    (err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text)
  )
);

/**
 * @api {post} /api/v1/project/:project_id/amendment/:amendment Create amendment
 * @apiVersion 1.0.0
 * @apiDescription Create a new amendment project (=> a new git branch).
 * @apiName Create amendment
 * @apiGroup Amendment
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (URL) {String} amendment the amendment name
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "commit": {
 *         "author_email": "john@example.com",
 *         "author_name": "John Smith",
 *         "authored_date": "2012-06-27T05:51:39-07:00",
 *         "committed_date": "2012-06-28T03:44:20-07:00",
 *         "committer_email": "john@example.com",
 *         "committer_name": "John Smith",
 *         "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
 *         "short_id": "7b5c3cc",
 *         "title": "add projects API",
 *         "message": "add projects API",
 *         "parent_ids": [
 *           "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
 *         ]
 *       },
 *       "name": "newbranch",
 *       "merged": false,
 *       "protected": false,
 *       "developers_can_push": false,
 *       "developers_can_merge": false
 *     }
 */
app.post(
  '/api/v1/project/:project_id/amendment',
  (req, res) => request.post(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests'
    )
    .send({
      id: req.params.project_id,
      title: req.body.title,
      source_branch: req.body.sourceBranch,
      target_branch: req.body.targetBranch,
      labels: 'draft',
    })
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request.njk'))
);

/**
 * @api {delete} /api/v1/project/:project_id/amendment/:amendment Delete amendment
 * @apiVersion 1.0.0
 * @apiDescription Delete an amendment (aka a git branch) from a project.
 * @apiName Delete amendment
 * @apiGroup Amendment
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (URL) {String} amendment_id the amendment id
 *
 */
app.delete('/api/v1/project/:project_id/amendment/:amendment_id', proxy('localhost:8000', {
  proxyReqOptDecorator: proxyAuthorizationHeader,
  proxyReqPathResolver: (req) => {
    return '/admin/api/v4/projects/' + req.params.project_id
      + '/merge_requests/' + req.params.amendment_id;
  },
}));

/**
 * @api {get} /project/:project_id/amendement amendments list of the projects (= branches) of the specified project
 * @apiVersion 1.0.0
 * @apiDescription Get the amendments list of projects (= branches) of the specified project
 * @apiName Get all amendments
 * @apiGroup Amendments
 *
 * @apiSuccess {String} content of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "name": "first-amendement",
 *        "commit": {
 *          "id": "e0da1d77801e85769ec32557511d946fcb4a202e",
 *          "short_id": "e0da1d77",
 *          "title": "Add the second article",
 *          "created_at": "2017-11-20T21:28:26.000+00:00",
 *          "parent_ids": [
 *            "70b25928f3ef88b544a444711d987b209de36786"
 *          ],
 *          "message": "Add the second article",
 *          "author_name": "Hugo",
 *          "author_email": "barthelemyhugo@gmail.com",
 *          "authored_date": "2017-11-20T21:28:26.000+00:00",
 *          "committer_name": "Hugo",
 *          "committer_email": "barthelemyhugo@gmail.com",
 *          "committed_date": "2017-11-20T21:28:26.000+00:00"
 *        },
 *        "merged": false,
 *        "protected": false,
 *        "developers_can_push": false,
 *        "developers_can_merge": false
 *      },
 *      {
 *       "name": "master",
 *       "commit": {
 *         "id": "e0da1d77801e85769ec32557511d946fcb4a202e",
 *         "short_id": "e0da1d77",
 *         "title": "Add the second article",
 *         "created_at": "2017-11-20T21:28:26.000+00:00",
 *         "parent_ids": [
 *           "70b25928f3ef88b544a444711d987b209de36786"
 *         ],
 *         "message": "Add the second article",
 *         "author_name": "Hugo",
 *         "author_email": "barthelemyhugo@gmail.com",
 *         "authored_date": "2017-11-20T21:28:26.000+00:00",
 *         "committer_name": "Hugo",
 *         "committer_email": "barthelemyhugo@gmail.com",
 *         "committed_date": "2017-11-20T21:28:26.000+00:00"
 *       },
 *       "merged": false,
 *       "protected": true,
 *       "developers_can_push": false,
 *       "developers_can_merge": false
 *     },
 *      {
 *       "name": "second-amendement",
 *       "commit": {
 *         "id": "e0da1d77801e85769ec32557511d946fcb4a202e",
 *         "short_id": "e0da1d77",
 *         "title": "Add the second article",
 *         "created_at": "2017-11-20T21:28:26.000+00:00",
 *         "parent_ids": [
 *           "70b25928f3ef88b544a444711d987b209de36786"
 *         ],
 *         "message": "Add the second article",
 *         "author_name": "Hugo",
 *         "author_email": "barthelemyhugo@gmail.com",
 *         "authored_date": "2017-11-20T21:28:26.000+00:00",
 *         "committer_name": "Hugo",
 *         "committer_email": "barthelemyhugo@gmail.com",
 *         "committed_date": "2017-11-20T21:28:26.000+00:00"
 *       },
 *       "merged": false,
 *       "protected": false,
 *       "developers_can_push": false,
 *       "developers_can_merge": false
 *     }
 */
app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests/' + req.params.amendment_id
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request.njk'))
);

app.get(
  '/api/v1/project/:project_id/amendment',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request_array.njk'))
);

/**
 * @api {get} /user Get the currently authenticated user
 * @apiVersion 1.0.0
 * @apiDescription Get the currently authenticated user
 * @apiName Get user
 * @apiGroup User
 *
 * @apiSuccess {Object} the authenticated user
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Administrator",
 *       "username": "root",
 *       "state": "active",
 *       "avatar_url": "https://secure.gravatar.com/avatar/249d3d16937c76c40b67e0a15b802522?s=80&d=identicon",
 *       "web_url": "https://amenda.test/admin/root",
 *       "created_at": "2017-11-20T20:40:53.860Z",
 *       "bio": null,
 *       "location": null,
 *       "skype": "",
 *       "linkedin": "",
 *       "twitter": "",
 *       "website_url": "",
 *       "organization": null,
 *       "last_sign_in_at": "2017-12-27T08:25:20.078Z",
 *       "confirmed_at": "2017-11-20T20:40:53.533Z",
 *       "last_activity_on": null,
 *       "email": "admin@amenda.test",
 *       "theme_id": 1,
 *       "color_scheme_id": 1,
 *       "projects_limit": 100000,
 *       "current_sign_in_at": "2017-12-27T10:02:50.945Z",
 *       "identities": [],
 *       "can_create_group": true,
 *       "can_create_project": true,
 *       "two_factor_enabled": false,
 *       "external": false,
 *       "is_admin": true
 *     }
 */
app.get('/api/v1/user', proxy('localhost:8000', {
  proxyReqOptDecorator: proxyAuthorizationHeader,
  proxyReqPathResolver: (req) => {
    return '/admin/api/v4/user';
  }
}));

app.listen(3001, () => {
  console.log('API server listening on port 3001');
});
