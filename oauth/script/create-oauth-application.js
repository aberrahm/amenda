#!/usr/bin/env node

const puppeteer = require('puppeteer');
const argv = require('yargs-parser')(process.argv.slice(2));

(async () => {
  try {
    const browser = await puppeteer.launch({
      ignoreHTTPSErrors: true,
      // This script is running as root during Ansible provisioning because
      // its stdout is redirected to /etc/amenda/oauth.json, so Chrome
      // must run without the sandbox.
      // https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#chrome-headless-fails-due-to-sandbox-issues
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    await page.setViewport({width: 128, height: 128});
    await page.goto('https://127.0.0.1/admin/users/sign_in');

    const user_login = await page.$('#user_login');
    const user_password = await page.$('#user_password');
    const login_submit_btn = await page.$('.login-body .btn-save');

    await user_login.type('root');
    await user_password.type('adminadmin');
    await login_submit_btn.click();
    await page.waitForNavigation();

    await page.goto('https://127.0.0.1/admin/admin/applications/new');

    const application_name = await page.$('#doorkeeper_application_name');
    const application_redirect_uri = await page.$('#doorkeeper_application_redirect_uri');
    const application_trusted = await page.$('#doorkeeper_application_trusted');
    const application_scopes_api = await page.$('#doorkeeper_application_scopes_api');
    const application_scopes_read_user = await page.$('#doorkeeper_application_scopes_read_user');
    const application_scopes_sudo = await page.$('#doorkeeper_application_scopes_sudo');
    const application_scopes_openid = await page.$('#doorkeeper_application_scopes_openid');
    const application_submit_btn = await page.$('#new_doorkeeper_application .btn-save');

    await application_name.type(argv['name']);
    await application_redirect_uri.type(argv['redirect-urls'].split(',').join('\n'));
    await application_trusted.click();
    await application_scopes_api.click();
    await application_scopes_read_user.click();
    await application_scopes_sudo.click();
    await application_scopes_openid.click();
    await application_submit_btn.click();
    await page.waitForNavigation();

    const application = await page.evaluate(() => {
      var application_id = document.getElementById('application_id');
      var application_secret = document.getElementById('secret');

      return {
        app_id: application_id.innerHTML,
        app_secret: application_secret.innerHTML,
      }
    });

    console.log(JSON.stringify(application, null, 2));

    await browser.close();
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
