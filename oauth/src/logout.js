module.exports = function(req, res) {
  req.session = null;

  if (req.query.redirect) {
    res.redirect(302, req.query.redirect);
  }

  res.status(200).send();
}
