# Getting started

In order to develop Amenda, you'll have to install your very own development environment.
To make this simpler/easier, the development environment is virtualized and its configuration described in code.
This approach is also important in order to:

* guarantee all developers have the exact same environment ;
* be OS/hardware independent ;
* reproduce as much as possible the actual production environment.

This documentation will help you in setting up a development environment for Amenda.

## Required configuration

* RAM : 4GB+ available
* Disk: 7GB+ available
* OS : Windows, macOS or Linux
* Internet access

## Installation

### Simple installation

The following procedure will automatically create, setup and configure the development
environment in a dedicated virtual machine:

1. Install [Vagrant 2.0+](https://www.vagrantup.com/).
2. Install [VirtualBox 5.0+](https://www.virtualbox.org/).
3. Clone the repository: `git clone git@framagit.org:parlement-ouvert/amenda.git`.
4. Enter the repository directory: `cd amenda`.
5. Create and provision the virtual machine: `vagrant up`;
 * during this step, a new dedicated virtual machine will be created, setup and all the dependencies will be installed;
 * Windows users *must* run this command as an administrator because of [this issue](https://www.virtualbox.org/ticket/10085)
6. [Edit your `hosts` file](https://www.rackspace.com/knowledge_center/article/modify-your-hosts-file) to add the
following entry: `192.168.50.42 amenda.test`.

If the installation process stops before completion for any reason, you can restart it with the `vagrant provision`
command.

*Note:* the installation will download a lot of heavy packages. The installation speed depends mostly on your Internet
bandwidth.

### Custom installation

#### Select the roles to install

Roles are grouped by tags.
It's possible to specify which roles are to be installed or to add roles to an existing
installation by specifying which tags are to be included/excluded.

The step 5. in the above procedure can be slightly modified in order to indicate which tags -
and the corresponding roles - are to be installed. Two variable environments are
available:

* `ANSIBLE_TAGS` to specify the exact list of specific tags are to be included
* `ANSIBLE_SKIP_TAGS` to specify which tags are to be skipped

The list of all available tags and the associated roles can be found in the  `/provisioning/playbook.yml` file.

Example for a new installation:

* `ANSIBLE_SKIP_TAGS=app,doc vagrant up` will skip all roles with *at least* the `doc` or `app` tag;
* `ANSIBLE_TAGS=api vagrant up` will install only the roles with *at least* the `api` tag.

To modify an existing installation, replace `vagrant up` with `vagrant provision`:
`ANSIBLE_TAGS=app vagrant provision` will add all the roles with *at least* the `app` tag to the
existing installation.

#### Installation logs

It's possible to make the installation process more verbose by defining the `ANSIBLE_VERBOSE` environment variable
before a `up` or a `provision`.

Example for a new installation:

```
ANSIBLE_VERBOSE=vvv vagrant up
```

## Check the installation

Open a web browser and go to the following URL: https://amenda.test/api/v1/ping. Once loaded, the Web page should show:

```json
{"ping":"pong"}
```

If the installation is not working:

* Destroy the development machine: `vagrant destroy`
* Redo the installation from scratch.

## API development

The API code is located in the `/api` directory.
Once the development environment installation is complete, you can edit the API code with your favorite IDE.
Modifying the code will automatically trigger:

* a rebuild of the API code;
* a restart of the API server with the latest changes.

## Web app development

The Web app code is located in the `/app` directory.
Once the development environment installation is complete, you can edit the Web app code with your favorite IDE.
Modifying the code will automatically trigger:

* a rebuild of the Web app code;
* a restart of the Web app development server to serve the latest changes;
* a refresh of the Web app in your Web browser to show the latest changes.

## Development logs

Development logs for the API and the Web app can be accessed in real-time by:

* connecting to the development virtual machine: `vagrant ssh`;
* running the `pm2 logs` command.

Those two commands can be abbreviated like so: `vagrant ssh -c "pm2 logs"`

## Documentation

This very documentation is built by the installation process. It is then made available here:

https://amenda.test/doc/

To rebuild the documentation after some changes, use the following command:

```
ANSIBLE_TAGS=doc vagrant provision
```
